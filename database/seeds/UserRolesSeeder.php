<?php

use Illuminate\Database\Seeder;
use App\Models\UserRole;

class UserRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('user_roles')->truncate();
        UserRole::create([
            'role_slug' => 'administrator',
            'role_name' => 'Administrator',
        ]);
        UserRole::create([
            'role_slug' => 'user',
            'role_name' => 'User',
        ]);
    }
}
