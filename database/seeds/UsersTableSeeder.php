<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('users')->truncate();

            User::create([
                'name' => 'Admin',
                'lastname' => 'RootAdmin',
                'phone' => '0669533474',
                'country_id' => 3159,
                'city_id' => 4400,
                'email' => 'admin@admin.com',
                'password' => '$2y$10$A8OU2YZHBo9NYTisqlxOwefgnSFUmkiFS8fmrX4DpikkwNyFA8/Ye',
                'user_role_id' => 1,
            ]);

    }

}
