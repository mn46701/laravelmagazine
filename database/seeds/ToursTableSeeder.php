<?php

use Illuminate\Database\Seeder;
use App\Tour;

class ToursTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('tours')->truncate();
        $i = 1;
        while ($i < 9) {
            Tour::create([
                'name' => 'tour ' . $i,
                'content' => 'Lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet',
                'price' => 100 + $i * 3,
                'tour_duration' => '06/07/2018 - 06/07/2018',
                'max_persons' => '20'
            ]);
            $i++;
        }
    }
}

