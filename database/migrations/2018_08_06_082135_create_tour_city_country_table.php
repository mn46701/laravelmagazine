<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourCityCountryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_city_country', function (Blueprint $table) {
            $table->integer('tour_id')->unsigned();
            $table->integer('country_country_id')->unsigned();
            $table->integer('city_id')->unsigned();
            $table->integer('tour_start')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
