<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->integer('parent_cat_id')->default(0);
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('tour_category', function (Blueprint $table) {
            $table->integer('tour_id')->unsigned();
            $table->integer('tour_category_id')->unsigned();
            $table->integer('active')->default(1);
            $table->primary(['tour_id', 'tour_category_id', 'active']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_category');
        Schema::dropIfExists('categories');
    }
}
