<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationsToTourCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tour_category', function (Blueprint $table) {
            $table->foreign('tour_id')->references('id')->on('tours');
            $table->foreign('tour_category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tour_category', function (Blueprint $table) {
            $table->dropForeign('tour_category_tour_id_foreign');
            $table->dropForeign('tour_category_tour_category_id_foreign');
        });
    }
}
