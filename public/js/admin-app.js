jQuery(document).ready(function () {

    jQuery('.menu-btn').click(function (e) {
        e.preventDefault();
        jQuery(this).toggleClass('menu-btn_active');
        jQuery('.sidebar_wrap').toggleClass('minified');
        jQuery('#app_wrap').toggleClass('open_full');
        jQuery('.sidebar_wrap a').each(function () {
            jQuery(this).find('.text').toggleClass('minified');
            jQuery(this).addClass('minified');
        });
    });

    var dateToday = new Date();
    if (jQuery('input[name="tour_duration"]').length !== 0) {
        jQuery('input[name="tour_duration"]').daterangepicker({
            minDate: dateToday,
            locale: {
                format: "DD/MM/YYYY",
            },
            opens: 'top'
        });
    }

    jQuery('#main_image').change(function () {
        jQuery('.main_image_upload_wrapper .loader_wrapper').css('display', 'flex');
        var reader = new FileReader();
        reader.readAsDataURL(this.files[0]);
        reader.onload = function (e) {
            jQuery('.upload_image_preview').css('background-image', 'url("' + e.target.result + '")');
            jQuery('.main_image_upload_wrapper .loader_wrapper').css('display', 'none');
        }
    });

    jQuery('#photos').change(function () {
        jQuery('#dropContainer .img_dropped').remove();
        jQuery(this.files).each(function (i) {
            var reader = new FileReader();
            reader.readAsDataURL(this);
            reader.onload = function (e) {
                jQuery('#dropContainer').append('<div class="img_dropped"><img src="' + e.target.result + '" width="100" height="100"></div>');
            }
        });
        jQuery('.drugAndDropWrapper').css('display', 'none');
    });

    jQuery('body').on('click', '.check_image', function () {
        jQuery(this).closest('.uploaded_image_buttons_wrapper').toggleClass('active');
        var choosed = 0;
        jQuery('.check_image').each(function () {
            if (jQuery(this).prop('checked')) {
                ++choosed;
            }
        });
        if (choosed > 0) {
            jQuery('.delete_images_button_wrapper .btn_wrap').addClass('active');
            jQuery('.delete_images_button_wrapper .btn_wrap span.image_to_del_count').html(choosed);
        } else {
            jQuery('.delete_images_button_wrapper .btn_wrap').removeClass('active');
        }
    });

    jQuery('body').on('click', '.delete_images_button_wrapper .btn_wrap button', function (e) {
        e.preventDefault();
        jQuery('.uploaded_images_wrapper').find('.loader_wrapper').css('display', 'flex');

        var ids = [];
        jQuery('.check_image').each(function () {
            if (jQuery(this).prop('checked')) {
                ids.push(jQuery(this).val());
            }
        });
        var data = {
            '_token': $('input[name=_token]').val(),
            'ids': JSON.stringify(ids),
            'tour_id': jQuery('#tour_id').val()
        };

        jQuery.ajax({
            type: 'POST',
            url: "/admin/images/delete",
            data: data,
            success: function (response) {
                jQuery('#allTourImageWrapper').html(response)
            }
        })

    });




    jQuery('#testApi').submit(function(e){
        e.preventDefault();


        jQuery.ajaxSetup({
            headers: {
                'X-CSRF-Token': jQuery('input[name=_token]').val()
            }
        });

        jQuery.ajax({
            url: '/api/connect/test',
            method: 'POST',
            contentType: "application/json",
            dataType: "json",
            data: {
                'asdasd': "asdasd"
            },
            success: function (response) {
                console.log(response);
            }
        })

    });

    jQuery('#multiselect').multiSelect();


    jQuery('#country_start').change(function(){

        jQuery('.choose_country_city .loader_wrapper').css('display', 'flex');

        jQuery.ajaxSetup({
            headers: {
                'X-CSRF-Token': jQuery('input[name=_token]').val()
            }
        });

        jQuery.ajax({
            url: '/cities/country/' + jQuery(this).val() + '/start',
            method: 'GET',
            success: function (response) {
                jQuery('.city_start').html(response);
                jQuery('.choose_country_city .loader_wrapper').hide();
            }
        })


    });

    jQuery('#country_end').change(function(){

        jQuery('.choose_country_city .loader_wrapper').css('display', 'flex');

        jQuery.ajaxSetup({
            headers: {
                'X-CSRF-Token': jQuery('input[name=_token]').val()
            }
        });

        jQuery.ajax({
            url: '/cities/country/' + jQuery(this).val() + '/end',
            method: 'GET',
            success: function (response) {
                jQuery('.city_end').html(response);
                jQuery('.choose_country_city .loader_wrapper').hide();
            }
        })


    });



});