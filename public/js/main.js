jQuery(document).ready(function(){

    jQuery('#country_id').change(function(){

        jQuery.ajaxSetup({
            headers: {
                'X-CSRF-Token': jQuery('input[name=_token]').val()
            }
        });

        jQuery.ajax({
            url: '/cities/' + jQuery(this).val() + '/get',
            method: 'GET',
            success: function (response) {
                jQuery('#city_id').removeAttr('disabled').html(response);
            }
        })

    });

});

