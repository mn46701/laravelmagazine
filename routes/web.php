<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\TourCategory;
use App\Models\Country;

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'UserController@index')->name('profile');
Route::resource('user', 'UserController');
Route::group(['middleware' => 'is_admin'], function () {
    Route::get('/admin', 'AdminController@dashboard')->name('admin');
    Route::get('/admin/tours', 'TourController@adminToursAll')->name('admintours');
    Route::get('/admin/tour/create', function () {
        $allCategories = TourCategory::all();
        $countriesAll = Country::all();
        return view('admin.templates.tours.tour-create', [
            'allCategories' => $allCategories,
            'countriesAll' => $countriesAll
        ]);
    })->name('create-tour-view');
    Route::post('/admin/tour/create', 'TourController@createTour')->name('create-tour');
    Route::get('/admin/tour/{tour}/delete', 'TourController@deleteTour');
    Route::get('/admin/tour/{tour}/edit', 'TourController@editTour')->name('edit-tour');
    Route::post('/admin/tour/{tour}/store', 'TourController@storeTour');
    Route::get('/admin/tour/{tour}/image/{image}/delete', 'TourController@deleteImage')->name('deleteTourImage');
    Route::post('/admin/tour/{tour}/image/{image}/change', 'TourController@changeImage')->name('changeTourImage');
    Route::post('/admin/images/delete', 'TourController@ajaxDeleteImages');

    Route::get('/admin/tours/categories', 'TourController@toursCategoriesAll')->name('toursCategoriesAll');

    Route::get('/admin/tours/category/create', function () {
        $categoriesAll = TourCategory::all();
        return view('admin.templates.tours.create-tour-category',
            [
                'categoriesAll' => $categoriesAll,
            ]);
    })->name('create-tour-category');

    Route::post('/admin/tours/category/create', 'TourController@storeCategory')->name('store-tour-category');
    Route::get('/admin/tours/category/{id}/edit', 'TourController@editTourCategory'); //edit view
    Route::post('/admin/tours/category/{id}/store', 'TourController@storeCategory')->name('store-edited-tour-category'); //store edited
    Route::get('/admin/tours/category/{id}/delete', 'TourController@deleteCategory'); //store edited

    Route::get('/admin/tours/filter', 'TourController@filterTours')->name('tours-filtration');
    Route::get('/admin/tours/search', 'TourController@filterTours')->name('tours-search');

    Route::get('/cities/country/{country_id}/{event}', 'TourController@getCities');

    Route::get('/admin/users/show', 'UserController@showAll')->name('users-show-all');
    Route::get('/admin/user/{id}/delete', 'UserController@destroy');
    Route::get('/admin/user/{id}/edit', 'UserController@show');
    Route::post('/admin/user/store/{id}', 'UserController@store')->name('userStore');

});

Route::get('/cities/{id}/get', 'UserController@getCities');


Route::get('/api/connect', 'ApiController@connectTestView')->name('connectTestView');
Route::post('/api/connect/test', 'ApiController@connect')->middleware('apiMiddleWare');
Route::post('/api/getAllTours', 'ApiController@getAllTours')->middleware('apiMiddleWare');
