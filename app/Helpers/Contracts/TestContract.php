<?php
namespace App\Helpers\Contracts;
use Illuminate\Http\Request;

interface TestContract
{

    public function getData(Request $request);
    public function getString();
    public function setString($string);

}