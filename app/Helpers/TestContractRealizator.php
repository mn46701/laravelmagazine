<?php
namespace App\Helpers;

use Illuminate\Http\Request;
use App\Helpers\Contracts\TestContract;

class TestContractRealizator implements TestContract{

    public $string = 'String';

    public function getData(Request $request){

        return $request->all();

    }

    public function getString()
    {
        return $this->string;
    }

    public function setString($string)
    {
        $this->string = $string;
    }

}