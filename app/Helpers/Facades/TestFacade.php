<?php
namespace App\Helpers\Facades;

use Illuminate\Support\Facades\Facade;

class TestFacade extends Facade
{

    public static function getFacadeAccessor()
    {
        return 'testcontract';
    }

    public static function getString()
    {
        return 'String';
    }

}