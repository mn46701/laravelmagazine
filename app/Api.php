<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Api extends Model
{
    public function scopeGetArr($query)
    {
        $arr = [
            'param1' => 'value1',
            'param2' => 'value2',
            'param3' => 'value3',
        ];
        return json_encode($arr);
    }

}
