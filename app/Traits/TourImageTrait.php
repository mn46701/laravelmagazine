<?php

namespace App\Traits;

use App\Http\Requests\StoreTourRequest;
use App\TourImage;
use App\Tour;
use App\Models\TourCategory;

trait TourImageTrait
{

    public function setMainImage(Tour $tour, StoreTourRequest $request)
    {
        if ($request->main_image !== null) {
            $tour->unsetMainImage();
            $main_image_path = $request->main_image->store('img/tourimages');
            TourImage::create([
                'tour_id' => $tour->id,
                'path' => $main_image_path,
                'tour_main_image' => 1
            ]);
        }
    }

    public function setAdditionalImages(Tour $tour, StoreTourRequest $request)
    {
        if ($request->photos !== null) {
            foreach ($request->photos as $photo) {
                $filename = $photo->store('img/tourimages');
                TourImage::create([
                    'tour_id' => $tour->id,
                    'path' => $filename
                ]);
            }
        }
    }

    public function setTourCategories(Tour $tour, StoreTourRequest $request)
    {
        if (isset($request->category_id) AND !empty($request->category_id)) {
            $data_to_sync = [];
            foreach ($request->category_id as $cat_id) {

                if ( isset($request->category_active) AND (array_search($cat_id, $request->category_active) !== false) )
                {
                    $data_to_sync[$cat_id] = ['active' => 1];
                } else {
                    $data_to_sync[$cat_id] = ['active' => 0];
                }
            }
            $tour->category()->sync($data_to_sync);
        } else {
            if (count($tour->category) > 0) {
                $tour->category()->detach();
            }
        }
    }

}