<?php

namespace App;

use App\Models\TourCategory;
use Illuminate\Database\Eloquent\Model;
use App\TourImage;
use Illuminate\Database\Eloquent\Builder;

class Tour extends Model
{
    protected $fillable = [
        'name', 'content', 'price', 'tour_duration', 'max_persons', 'booked'
    ];

    public function getMainImage()
    {
        $image = $this->images()->where('tour_main_image', '=', '1')->first();
        return $image;
    }

    public function getMainImagePath()
    {
        $main_image = $this->getMainImage();
        if ($main_image !== null) {
            return '/public/' . $main_image->path;
        } else {
            return '/public/img/tour.png';
        }
    }

    public function unsetMainImage()
    {
        $image = $this->getMainImage();
        if ($image !== null) {
            $image->fill(['tour_main_image' => 0])->save();
        }
    }

    public function getTourDescriptionImages()
    {
        $TourDescriptionImages = $this->images()->where('tour_main_image', '<>', '1')->get();
        return $TourDescriptionImages;
    }

    public function images()
    {
        return $this->hasMany('App\TourImage', 'tour_id', 'id');
    }

    public function words_limit($input_text, $limit = 50, $end_str = '')
    {
        $input_text = strip_tags($input_text);
        $words = explode(' ', $input_text); // создаём из строки массив слов
        if ($limit < 1 || sizeof($words) <= $limit) { // если лимит указан не верно или количество слов меньше лимита, то возвращаем исходную строку
            return $input_text;
        }
        $words = array_slice($words, 0, $limit); // укорачиваем массив до нужной длины
        $out = implode(' ', $words);
        return $out . $end_str; //возвращаем строку + символ/строка завершения
    }

    public function delete()
    {
        foreach ($this->images()->get() as $image)
        {
            TourImage::find($image->id)->delete();
        }
        parent::delete(); // TODO: Change the autogenerated stub
    }

    public function category()
    {
        return $this->belongsToMany('App\Models\TourCategory', 'tour_category')
            ->withPivot('active');
    }

    public function country()
    {
        return $this->belongsToMany('App\Models\Country', 'tour_city_country')
            ->withPivot('tour_start');
    }


    public function scopeIsActive()
    {
        if ( $this->category()->wherePivot('active', '=', '1')->exists() )
        {
            return true;
        } else {
            return false;
        }
    }

}
