<?php

namespace App\Providers;

use App\Helpers\TestContractRealizator;
use Illuminate\Support\ServiceProvider;

class TestContractServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('App\Helpers\Contracts\TestContract', function(){
            return new TestContractRealizator();
        });
    }
}
