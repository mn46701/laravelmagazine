<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'city';

    protected $fillable = [
        'name'
    ];

    public $primaryKey = 'city_id';

    public function tour()
    {
        return $this->belongsToMany('App\Tour', 'tour_city_country')
            ->withPivot('tour_start');
    }
}
