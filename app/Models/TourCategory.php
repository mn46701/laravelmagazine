<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TourCategory extends Model
{
    protected $table = 'categories';
    protected $fillable = [
        'slug', 'name', 'parent_cat_id'
    ];

    public function childs()
    {
        return $this->hasMany('App\Models\TourCategory', 'parent_cat_id', 'id');
    }

    public function tour()
    {
        return $this->belongsToMany('App\Tour', 'tour_category')
            ->withPivot('active');
    }

}
