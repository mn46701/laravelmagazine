<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'country';

    protected $fillable = [
        'name'
    ];

    public $primaryKey = 'country_id';

    public function cities()
    {
        return $this->hasMany('App\Models\City', 'country_id', 'country_id');
    }

    public function tour()
    {
        return $this->belongsToMany('App\Tour', 'tour_city_country')
            ->withPivot('tour_start');
    }

}
