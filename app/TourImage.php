<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourImage extends Model
{
    protected $fillable = [
        'tour_id', 'path', 'tour_main_image'
    ];

    public function getImagePath()
    {
        return '/public/' . $this->path;
    }

    public function tour()
    {
        return $this->belongsTo('App\Tour');
    }

    public function delete()
    {
        @unlink(public_path($this->path));
        parent::delete();
    }
}
