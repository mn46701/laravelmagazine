<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTourRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'content' => 'required',
            'main_image' => 'nullable|image|mimes:jpeg,bmp,png,jpg|max:3000',
            'price' => 'required',
            'max_persons' => 'numeric|min:1',
        ];
        $photos = count($this->input('photos'));
        foreach(range(0, $photos) as $index) {
            $rules['photos.' . $index] = 'nullable|image|mimes:jpeg,bmp,png,jpg|max:3000';
        }
        return $rules;
    }
}
