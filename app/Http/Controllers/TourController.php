<?php

namespace App\Http\Controllers;


//use App\Helpers\Contracts\TestContract;


use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Requests\StoreTourRequest;
use App\Tour;
use App\TourImage;
use App\Models\TourCategory;
use App\Traits\TourImageTrait;
use App;
use App\Models\Country;


use TestFacade;

class TourController extends Controller
{
    use TourImageTrait;

    public function adminToursAll()
    {
        return view('admin.templates.tours.tours', [
            'tours' => Tour::all()->sortByDesc('created_at'),
            'title' => 'All tours',
            'allCategories' => TourCategory::all()
        ]);
    }

    public function deleteTour($id)
    {
        $tour = Tour::find($id);
        if (count($tour->category) > 0) {
            $tour->category()->detach();
        }
        $tour->delete();
        return redirect('/admin/tours');
    }

    public function editTour($id)
    {
        $tour = Tour::find($id);
        return view('admin.templates.tours.tour-edit', ['tour' => $tour, 'allCategories' => TourCategory::all()]);
    }

    public function createTour(StoreTourRequest $request/*, TestContract $contract */ )
    {

//        dd($request->all());

//        dd(TestFacade::getString());

//        $contract = App::make('App\Helpers\Contracts\TestContract');
//        dump($contract->getString());
//        $contract->setString('another_string');
//        dump($contract->getString());
//        $contract_new_obj = App::make('App\Helpers\Contracts\TestContract');
//        dd($contract_new_obj);



        $tour = Tour::create($request->all());
        $tour->country()->sync([
            $request->country_start => [
                'city_id' => $request->city_start,
                'tour_start' => 1
            ],
            $request->country_end => [
                'city_id' => $request->city_end
            ]
        ]);

        $this->setTourCategories($tour, $request);
        $this->setMainImage($tour, $request);
        $this->setAdditionalImages($tour, $request);
        return redirect()->route('edit-tour', [
            'tour' => $tour->id,
            'success' => true,
            'action' => 'create_tour'
        ]);
    }

    public function storeTour(StoreTourRequest $request, $id)
    {
        $tour = Tour::find($id);
        $tour->fill($request->all())->save();
        $this->setTourCategories($tour, $request);
        $this->setMainImage($tour, $request);
        $this->setAdditionalImages($tour, $request);
        return redirect()->route('edit-tour', [
            'tour' => $tour->id,
            'success' => true,
            'action' => 'update_tour'
        ]);
    }

    public function deleteImage($tour_id, $image_id) //id image
    {
        $image = TourImage::find($image_id)->delete();
        return redirect()->route('edit-tour', ['tour' => $tour_id]);
    }

    public function ajaxDeleteImages()
    {
        $tour_id = Input::get('tour_id');
        $ids = json_decode(Input::get('ids'));
        foreach ($ids as $id) {
            TourImage::find($id)->delete();
        }
        return view('admin.templates.tours.tour-uploaded-images', ['tour' => Tour::find($tour_id)]);
    }

    public function toursCategoriesAll()
    {
        return view('admin.templates.tours.categories', ['categories' => TourCategory::all()]);
    }

    public function storeCategory(Request $request, $id = null)
    {
        $saveArr = $request->all();
        $implodedArrSlug = explode(' ', $request->name);
        $slug = '';
        foreach ($implodedArrSlug as $item) {
            $slug .= $item . '_';
        }
        $saveArr['slug'] = mb_strtolower(substr($slug, 0, -1));
        if ($id == null) {
            TourCategory::create($saveArr);
        } else {
            $cat = TourCategory::find($id);
            $cat->fill($saveArr)->save();
        }
        return redirect()->route('toursCategoriesAll');
    }

    public function editTourCategory($id)
    {
        return view('admin.templates.tours.editing-category', [
            'category' => TourCategory::find($id),
            'categoriesAll' => TourCategory::all()
        ]);
    }

    public function deleteCategory($id)
    {
        TourCategory::find($id)->delete();
        return redirect()->route('toursCategoriesAll');
    }

    public function filterTours(Request $request)
    {
        $query = Tour::query();
        $query->when(request('price_from'), function ($q) use ($request) {
            return $q->where('price', '>', $request->price_from);
        });

        $query->when(request('price_to'), function ($q) use ($request) {
            return $q->where('price', '<', $request->price_to);
        });

        $query->when(request('category') > 0, function ($q) use ($request) {
            return $q->whereHas('category', function ($query) use ($request) {
                $query->where('tour_category_id', $request->category);
            });
        });

        $query->when(request('search'), function ($q) use ($request) {
            return $q->where('name', 'LIKE', '%' . $request->search . '%');
        });

        $query->orderByDesc('created_at');

        return view('admin.templates.tours.tours', [
            'tours' => $query->get(),
            'title' => 'Filtered',
            'allCategories' => TourCategory::all()
        ]);
    }

    public function getCities($country_id, $event)
    {
        $cities = Country::find($country_id)->cities;
        return view('admin.templates.tours.geo.city', ['cities' => $cities, 'event' => $event]);
    }

}
