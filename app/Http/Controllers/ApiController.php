<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Api;
use App\Tour;

class ApiController extends Controller
{

    public function connect( Request $request )
    {
        $jsonarr = Api::GetArr($request);
        dd($jsonarr);
    }

    public function connectTestView()
    {
        return view('testapi');
    }

    public function getAllTours()
    {
        return json_encode(Tour::all());
    }

}
