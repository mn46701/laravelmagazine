<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'lastname',
        'phone',
        'country_id',
        'city_id',
        'email',
        'password',
        'user_role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin()
    {
        if ( $this->role()->first()->role_slug === 'administrator' )
        {
            return true;
        } else {
            return false;
        }
    }

    public function role()
    {
        return $this->hasOne('App\Models\UserRole', 'id', 'user_role_id');
    }

    public function country()
    {
        return $this->hasOne('App\Models\Country', 'country_id', 'country_id');
    }

    public function city()
    {
        return $this->hasOne('App\Models\City', 'city_id', 'city_id');
    }
}
