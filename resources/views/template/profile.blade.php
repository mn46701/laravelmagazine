@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="col-md-3">
                    <ul class="nav av-pills nav-stacked">
                        <li class="active"><a data-toggle="tab" href="#home">User</a></li>
                        <li><a data-toggle="tab" href="#menu1">Order</a></li>
                        {{--<li><a data-toggle="tab" href="#menu2">Menu 2</a></li>--}}
                        {{--<li><a data-toggle="tab" href="#menu3">Menu 3</a></li>--}}
                    </ul>
                </div>
                <div class="col-md-9">
                    <div class="tab-content">
                        <div id="home" class="tab-pane fade in active">
                            @include('template.profile-bloc.user_date')
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            @include('template.profile-bloc.user-order')
                        </div>
                        {{--<div id="menu2" class="tab-pane fade">--}}
                            {{--<h3>Menu 2</h3>--}}
                            {{--<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque--}}
                                {{--laudantium, totam rem aperiam.</p>--}}
                        {{--</div>--}}
                        {{--<div id="menu3" class="tab-pane fade">--}}
                            {{--<h3>Menu 3</h3>--}}
                            {{--<p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt--}}
                                {{--explicabo.</p>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>


        </div>
    </div>
@endsection
