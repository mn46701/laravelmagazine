@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel-body">
            <h1>
                Edit
            </h1>
            {!! Form::model($user, ['method' => 'PATCH','route' => ['user.update', $user->id],'files' => true]) !!}
            {!! Form::label('name') !!}
            {!! Form::text('name', null,['class'=>'form-control']) !!}
            {!! Form::label('lastname') !!}
            {!! Form::text('lastname', null,['class'=>'form-control']) !!}
            {!! Form::label('phone') !!}
            {!! Form::text('phone', null,['class'=>'form-control']) !!}

            {!! Form::label('country') !!}

            <select name="country_id" id="country_id" class="select_on_reg">
                <option value="{{$user->country->country_id}}">{{$user->country->name}}</option>
            </select>

            {!! Form::label('city') !!}

            <select name="city_id" id="city_id" class="select_on_reg">
                <option value="{{$user->city->city_id}}">{{$user->city->name}}</option>
            </select>



            {!! Form::label('email') !!}
            {!! Form::text('email', null,['class'=>'form-control']) !!}
            <br>
            {!! Form::submit('Edit', array('class' => 'btn btn-lg btn-primary btn-block')) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection