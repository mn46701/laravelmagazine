<h3>Uploading images</h3>
<div id="dropContainer">
    <div class="whenFileOn">
        <h1>Drop Files Here</h1>
    </div>
    <div class="loader_wrapper">
        <div class="sk-cube-grid">
            <div class="sk-cube sk-cube1"></div>
            <div class="sk-cube sk-cube2"></div>
            <div class="sk-cube sk-cube3"></div>
            <div class="sk-cube sk-cube4"></div>
            <div class="sk-cube sk-cube5"></div>
            <div class="sk-cube sk-cube6"></div>
            <div class="sk-cube sk-cube7"></div>
            <div class="sk-cube sk-cube8"></div>
            <div class="sk-cube sk-cube9"></div>
        </div>
    </div>
    <div class="drugAndDropWrapper">
        <h3>Drop images here, or</h3>
        <label for="photos" class="upload_photos_label">Upload images</label>
        <input type="file" name="photos[]" id="photos" multiple="multiple">
    </div>
</div>

<script>
    var dropContainer = document.getElementById('dropContainer');
    var fileInput = document.getElementById('photos');
    dropContainer.ondragover = dropContainer.ondragenter = function (evt) {
        evt.preventDefault();
        jQuery('.whenFileOn').css('display', 'flex');
    };
    dropContainer.ondrop = function (evt) {
        fileInput.files = evt.dataTransfer.files;
        evt.preventDefault();
        jQuery('.whenFileOn').css('display', 'none');
        jQuery('.drugAndDropWrapper').css('display', 'none');
    };
</script>