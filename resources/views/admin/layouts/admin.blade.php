<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Admin Panel</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css"
          integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="{{ asset('public/css/bootstrap.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('public/css/multi-select.css') }}">
    <link href="{{ asset('public/css/admin-app.css') }}" rel="stylesheet">
</head>
<body>

<div class="container-fluid header_wrap">
    <div class="header">
        <a href="#" class="menu-btn menu-btn_active">
            <span></span>
        </a>

        <a href="{{ route('profile') }}">
            <img src="{{ asset('public/img/logo.jpg') }}" alt="">
        </a>

    </div>
</div>

<div class="container-fluid">
    <div class="sidebar_wrap col-xs-2">

        <ul class="admin-main-menu">
            <li>
                <a href="{{ route('admin') }}" class="menu_link">
                    <i class="fas fa-tachometer-alt"></i>
                    <span class="text">Dashboard</span>
                </a>
            </li>
            <li>
                <a href="{{ route('admintours') }}" class="menu_link">
                    <i class="fas fa-plane-departure"></i>
                    <span class="text">Tours</span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('admintours') }}" class="menu_link">
                            <i class="fas fa-plane-departure"></i>
                            <span class="text">Show all</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('create-tour-view') }}" class="menu_link">
                            <i class="fas fa-plane-departure"></i>
                            <span class="text">Create new</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('toursCategoriesAll') }}" class="menu_link">
                            <i class="fas fa-plane-departure"></i>
                            <span class="text">Categories</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="/admin/users/show" class="menu_link">
                    <i class="fas fa-users"></i>
                    <span class="text">Users</span>
                </a>
            </li>
        </ul>


    </div>
    <div id="app_wrap" class="col-xs-10">
        <div id="app" class="col-xs-12">
            @yield('content')
        </div>
    </div>
</div>


<!-- Scripts -->

<script src="{{ asset('public/js/jquery.js') }}"></script>
<script src="{{ asset('public/js/jquery.multi-select.js') }}"></script>
@yield('scripts')
<script src="{{ asset('public/js/admin-app.js') }}"></script>
</body>
</html>
