@if(count($tour->getTourDescriptionImages()) > 0)
    <div class="col-xs-12"><h3>Uploaded images</h3></div>
    <div class="col-xs-12 uploaded_images_wrapper no_padding">
        <div class="loader_wrapper">
            <div class="sk-cube-grid">
                <div class="sk-cube sk-cube1"></div>
                <div class="sk-cube sk-cube2"></div>
                <div class="sk-cube sk-cube3"></div>
                <div class="sk-cube sk-cube4"></div>
                <div class="sk-cube sk-cube5"></div>
                <div class="sk-cube sk-cube6"></div>
                <div class="sk-cube sk-cube7"></div>
                <div class="sk-cube sk-cube8"></div>
                <div class="sk-cube sk-cube9"></div>
            </div>
        </div>
        @foreach($tour->getTourDescriptionImages() as $tourImage)
            <div class="col-md-3 uploaded_image_wrapper">
                <div class="uploaded_image" style="background-image: url('{{$tourImage->getImagePath()}}')">
                    <div class="change_image_wrapper">

                        <div class="loader_wrapper">
                            <div class="sk-cube-grid">
                                <div class="sk-cube sk-cube1"></div>
                                <div class="sk-cube sk-cube2"></div>
                                <div class="sk-cube sk-cube3"></div>
                                <div class="sk-cube sk-cube4"></div>
                                <div class="sk-cube sk-cube5"></div>
                                <div class="sk-cube sk-cube6"></div>
                                <div class="sk-cube sk-cube7"></div>
                                <div class="sk-cube sk-cube8"></div>
                                <div class="sk-cube sk-cube9"></div>
                            </div>
                        </div>

                    </div>

                    <div class="uploaded_image_buttons_wrapper">

                        <div class="check_image_wrap">
                            <label for="check_image_{{$tourImage->id}}"
                                   class="checkmark for_checkbox_label"></label>
                            <input type="checkbox" class="check_image regular-checkbox"
                                   id="check_image_{{$tourImage->id}}" value="{{$tourImage->id}}">
                        </div>

                        <a href="{{route('deleteTourImage', ['tour'=>$tour->id, 'image' => $tourImage->id])}}"
                           class="delete">
                            <span class="text">Delete</span>
                            <i class="fas fa-trash-alt"></i>
                        </a>

                    </div>
                </div>
            </div>
        @endforeach

        <div class="col-xs-12 delete_images_button_wrapper">
            <div class="btn_wrap">
                <button class="btn btn-danger">Delete images (<span class="image_to_del_count"></span>)
                </button>
            </div>
        </div>

    </div>
@endif