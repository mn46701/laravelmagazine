@extends('admin.layouts.admin')

@section('content')


    @if (count($errors) > 0)
        <ul class="errors_ul">
            @foreach ($errors->all() as $error)
                <li class="alert alert-danger">{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    @if(count($categories) > 0)

        <div class="margin_bottom">
            <a href="{{route('create-tour-category')}}" class="btn btn-default">Create Category</a>
        </div>

        @foreach($categories as $category)

            @if($category->parent_cat_id == 0)

                <div class="one_cat_wrapp">

                    <div class="one_cat col-xs-12">
                        {{$category->name}}
                        <span class="tours_count">({{count($category->tour)}})</span>

                        <div class="categories_controls">
                            <a href="/admin/tours/category/{{$category->id}}/edit" class="edit">
                                <span class="text">Edit</span>
                                <i class="fas fa-edit"></i>
                            </a>
                            <a href="/admin/tours/category/{{$category->id}}/delete" class="delete">
                                <span class="text">Delete</span>
                                <i class="fas fa-trash-alt"></i>
                            </a>
                        </div>
                    </div>

                    @if ( count($category->childs()->get()) > 0 )
                        @include('admin.templates.tours.categories.one-cat')
                    @endif

                </div>

            @endif

        @endforeach

    @else
        <div class="alert alert-danger no_tours_mes">
            <div>There are not anough categories. . .</div>
            <div>
                <a href="{{route('create-tour-category')}}" class="btn btn-default">Create Category</a>
            </div>
        </div>
    @endif




@endsection