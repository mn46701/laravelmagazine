@extends('admin.layouts.admin')

@section('content')

    <div class="creating_new_tour_wrapper col-xs-12">

        <h3>Creating new tour</h3>

        @if (count($errors) > 0)
            <ul class="errors_ul">
                @foreach ($errors->all() as $error)
                    <li class="alert alert-danger">{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        {{ Form::open( ['route' => 'create-tour', 'method' => 'POST', 'enctype' => 'multipart/form-data'] ) }}


        <div class="col-md-8 tour_block_creating_wrapper">
            <div class="tour_block_creating">
                <label for="tour_name">Tour name</label>
                <input type="text" name="name" id="tour_name">
                <label for="tour_content">Tour content</label>
                <textarea name="content" id="tour_content" cols="30" rows="10"></textarea>

                <div class="flex-row">
                    <div class="flex-60">
                        <label for="tour_duration">Tour duration</label>
                        <input type="text" name="tour_duration" id="tour_duration">
                    </div>

                    <div class="max_persons">
                        <label for="max_persons">Max persons</label>
                        <input type="number" id="max_persons" name="max_persons">
                    </div>

                    <div class="price_wrap">
                        <label for="price">Price, USD</label>
                        <i class="fas fa-dollar-sign"></i>
                        <input type="number" name="price" id="price" class="price-input">
                    </div>
                </div>

            </div>
        </div>

        <div class="col-md-4 tour_block_creating_wrapper main_image_upload_wrapper">

            <div class="loader_wrapper">
                <div class="sk-cube-grid">
                    <div class="sk-cube sk-cube1"></div>
                    <div class="sk-cube sk-cube2"></div>
                    <div class="sk-cube sk-cube3"></div>
                    <div class="sk-cube sk-cube4"></div>
                    <div class="sk-cube sk-cube5"></div>
                    <div class="sk-cube sk-cube6"></div>
                    <div class="sk-cube sk-cube7"></div>
                    <div class="sk-cube sk-cube8"></div>
                    <div class="sk-cube sk-cube9"></div>
                </div>
            </div>

            <div class="upload_image_preview" style="background-image: url('/public/img/noimage.jpeg');"></div>

            <input type="file" name="main_image" id="main_image">
            <label for="main_image" class="upload_image_button">Choose image</label>

        </div>

        <div class="col-xs-12 no_padding">
            <div class="col-md-4 border_wrapp category_wrapper">
                <h3>Choose category</h3>

                <div class="selected_and_unselected_cats">
                    <div>All categories</div>
                    <div>Selected categories</div>
                </div>

                <select name="category_id[]" id="multiselect" multiple="multiple">
                    @foreach($allCategories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>

            </div>

            <div class="col-md-4">
                <div class="border_wrapp col-xs-12 choose_country_city position_relative">
                    <div class="loader_wrapper">
                        <div class="sk-cube-grid">
                            <div class="sk-cube sk-cube1"></div>
                            <div class="sk-cube sk-cube2"></div>
                            <div class="sk-cube sk-cube3"></div>
                            <div class="sk-cube sk-cube4"></div>
                            <div class="sk-cube sk-cube5"></div>
                            <div class="sk-cube sk-cube6"></div>
                            <div class="sk-cube sk-cube7"></div>
                            <div class="sk-cube sk-cube8"></div>
                            <div class="sk-cube sk-cube9"></div>
                        </div>
                    </div>

                    <h3>Choose Countries</h3>

                    <div class="country_start">
                        @include('admin.templates.tours.geo.country', ['event' => 'start'])
                    </div>

                    <div class="city_start"></div>
                    <div class="country_end">
                        @include('admin.templates.tours.geo.country', ['event' => 'end'])
                    </div>
                    <div class="city_end"></div>

                </div>
            </div>
        </div>

        <div class="col-xs-12 no_padding">
            @include('layouts.upload-images-block')
        </div>

        <div class="col-xs-12 no_padding">
            <input type="submit" value="Create" class="btn btn-success margin_top">
        </div>

        {{ Form::close() }}


    </div>

@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('public/js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/daterangepicker.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/daterangepicker.css') }}"/>
@endsection