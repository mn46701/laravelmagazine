@extends('admin.layouts.admin')

@section('content')



    @if (count($errors) > 0)
        <ul class="errors_ul">
            @foreach ($errors->all() as $error)
                <li class="alert alert-danger">{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    {{ Form::open( ['route' => ['store-edited-tour-category', $category->id], 'method' => 'POST', 'id' => 'create_tour_category'] ) }}

    <input type="text" name="name" value="{{$category->name}}">
    <select name="parent_cat_id">

        @if($category->parent_cat_id !== 0)
            <option value="0">Select parent category...</option>
            @foreach($categoriesAll as $oneCat)

                @if($oneCat->id !== $category->id)
                    @if($oneCat->id == $category->parent_cat_id)
                        <option value="{{$oneCat->id}}" selected>{{$oneCat->name}}</option>
                    @else
                        <option value="{{$oneCat->id}}">{{$oneCat->name}}</option>
                    @endif
                @endif

            @endforeach
        @else
            <option value="0" selected>Select parent category...</option>
            @foreach($categoriesAll as $oneCat)
                @if($oneCat->id !== $category->id)
                    <option value="{{$oneCat->id}}">{{$oneCat->name}}</option>
                @endif
            @endforeach
        @endif


    </select>
    <input type="submit" class="btn btn-success" value="Save">

    {{ Form::close() }}


@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('public/js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/daterangepicker.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/daterangepicker.css') }}"/>
@endsection