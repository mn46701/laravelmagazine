@extends('admin.layouts.admin')

@section('content')


    @if (count($errors) > 0)
        <ul class="errors_ul">
            @foreach ($errors->all() as $error)
                <li class="alert alert-danger">{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <div class="col-xs-12">
        <h1 class="margin_bottom">Creating category</h1>
    </div>


    {{ Form::open( ['route' => 'store-tour-category', 'method' => 'POST', 'id' => 'create_tour_category'] ) }}

    <input type="text" name="name" placeholder="Category name">
    <select name="parent_cat_id">
        <option value="0" selected>Select parent category...</option>
        @foreach($categoriesAll as $oneCat)
            <option value="{{$oneCat->id}}">{{$oneCat->name}}</option>
        @endforeach
    </select>
    <input type="submit" class="btn btn-success" value="Create">

    {{ Form::close() }}




@endsection