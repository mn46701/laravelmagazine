<div class="one_tour">
    <div class="uploaded_image_wrapper no_margin">
        <div class="one_tour_image uploaded_image"
             style="background-image: url('{{ $tour->getMainImagePath() }}')">
            <div class="uploaded_image_buttons_wrapper">
                <a href="/admin/tour/{{$tour->id}}/delete" class="delete">
                    <span class="text">Delete</span>
                    <i class="fas fa-trash-alt"></i>
                </a>
                <a href="/admin/tour/{{$tour->id}}/edit" class="edit">
                    <span class="text">Change</span>
                    <i class="fas fa-edit"></i>
                </a>
            </div>
        </div>
    </div>


    <div class="one_tour_duration">
        {{$tour->tour_duration}}
        <div class="tour_active_mes">
            @if($tour->IsActive() === true)
                <div class="active">Active</div>
            @else
                <div class="disabled">Disabled</div>
            @endif
        </div>

    </div>


    <div class="one_tour_tit">
        {{ $tour->name  }}
    </div>

    <div class="one_tour_content">
        {{ $tour->words_limit($tour->content, 30, '. . .')  }}
    </div>

    @if(count($tour->category) > 0)
        <div class="one_tour_category">
            @foreach($tour->category as $category)
                <span class="one_cat_tour">{{$category->name}}</span>
            @endforeach
        </div>
    @endif

    <div class="one_tour_price">
        {{ $tour->price }} USD
    </div>


</div>