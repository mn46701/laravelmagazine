@extends('admin.layouts.admin')

@section('content')
    <div class="creating_new_tour_wrapper col-xs-12 no_padding">

        @if( isset($_GET['success']) AND $_GET['success'] == 1 AND count($errors) == 0)
            <div class="col-xs-12">
                @if(isset($_GET['action']) AND $_GET['action'] == 'update_tour')
                    <div class="alert alert-success">Tour saved</div>
                @elseif(isset($_GET['action']) AND $_GET['action'] == 'create_tour')
                    <div class="alert alert-success">Tour created</div>
                @endif
            </div>
        @endif

        @if (count($errors) > 0)
            <ul class="errors_ul">
                @foreach ($errors->all() as $error)
                    <li class="alert alert-danger">{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        {{ Form::open( ['url' => '/admin/tour/'. $tour->id .'/store', 'method' => 'POST', 'enctype' => 'multipart/form-data', 'id' => 'editingForm'] ) }}

        <div class="col-xs-12">
            <h2>Editing Tour:
                <bold>{{$tour->name}} </bold>
            </h2>
            <div class="col-md-8 tour_block_creating_wrapper">
                <div class="tour_block_creating">
                    <label for="tour_name">Tour name</label>
                    <input type="text" name="name" id="tour_name" value="{{$tour->name}}">
                    <label for="tour_content">Tour content</label>
                    <textarea name="content" id="tour_content" cols="30" rows="10">{{$tour->content}}</textarea>

                    <div class="flex-row">
                        <div class="one_tour_duration_wrap">
                            <label for="tour_duration">Tour duration</label>
                            <input type="text" name="tour_duration" id="tour_duration" value="{{$tour->tour_duration}}">
                        </div>

                        <div class="max_persons">
                            <label for="max_persons">Max persons</label>
                            <input type="number" id="max_persons" name="max_persons" value="{{$tour->max_persons}}">
                        </div>

                        <div class="price_wrap">
                            <label for="price">Price, USD</label>
                            <i class="fas fa-dollar-sign"></i>
                            <input type="number" name="price" id="price" class="price-input" value="{{$tour->price}}">
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-4 tour_block_creating_wrapper main_image_upload_wrapper">

                <div class="loader_wrapper">
                    <div class="sk-cube-grid">
                        <div class="sk-cube sk-cube1"></div>
                        <div class="sk-cube sk-cube2"></div>
                        <div class="sk-cube sk-cube3"></div>
                        <div class="sk-cube sk-cube4"></div>
                        <div class="sk-cube sk-cube5"></div>
                        <div class="sk-cube sk-cube6"></div>
                        <div class="sk-cube sk-cube7"></div>
                        <div class="sk-cube sk-cube8"></div>
                        <div class="sk-cube sk-cube9"></div>
                    </div>
                </div>

                <div class="uploaded_image_wrapper no_margin">
                    <div class="upload_image_preview" style="background-image: url('{{$tour->getMainImagePath()}}');">
                        @if($tour->getMainImage() !== null)
                            <div class="uploaded_image_buttons_wrapper">
                                <a href="{{route('deleteTourImage', ['tour'=>$tour->id, 'image' => $tour->getMainImage()->id])}}"
                                   class="delete">
                                    <span class="text">Delete</span>
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                                <a href="{{route('changeTourImage', ['tour'=>$tour->id, 'image' => $tour->getMainImage()->id])}}"
                                   class="edit">
                                    <span class="text">Change</span>
                                    <i class="fas fa-edit"></i>
                                </a>
                            </div>
                        @endif
                    </div>
                </div>

                <input type="file" name="main_image" id="main_image">
                <label for="main_image" class="upload_image_button">Choose image</label>

            </div>
        </div>

        <div class="col-xs-12">
            <div class="col-md-4 border_wrapp category_wrapper">
                <h3>Choose category</h3>

                <div class="selected_and_unselected_cats">
                    <div>All categories</div>
                    <div>Selected categories</div>
                </div>

                <select name="category_id[]" id="multiselect" multiple="multiple">

                    @if(count($tour->category) > 0)
                        @foreach($allCategories as $category)
                            @if($tour->category()->where('id', $category->id)->exists())
                                <option value="{{$category->id}}" selected>{{$category->name}}</option>
                            @else
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endif
                        @endforeach
                    @else
                        @foreach($allCategories as $category)
                            @if($category->id !== $tour->category_id)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endif
                        @endforeach
                    @endif

                </select>

            </div>
            <div class="col-md-4">
                <div class="col-md-12 border_wrapp active_categories_wrapper">
                    <h3>Choose active category</h3>
                    @foreach($tour->category as $category)
                        <div class="one_cat">
                            {{$category->name}}
                            <div class="check_active">
                                @if($category->pivot->active == 1)
                                    <input type="checkbox" name="category_active[]" value="{{$category->id}}" checked>
                                @else
                                    <input type="checkbox" name="category_active[]" value="{{$category->id}}">
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div id="allTourImageWrapper">
            @include('admin.templates.tours.tour-uploaded-images')
        </div>

        <div class="col-xs-12">
            @include('layouts.upload-images-block')
        </div>

        <div class="col-xs-12">
            <input type="submit" value="Save" class="btn btn-success margin_top">
        </div>

        {{ Form::close() }}

        <input type="hidden" id="tour_id" value="{{$tour->id}}">
    </div>

@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('public/js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/daterangepicker.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/daterangepicker.css') }}"/>
@endsection

