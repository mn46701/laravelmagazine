


    @if ( count($category->childs()->get()) > 0 )
        @foreach($category->childs()->get() as $category)

            <div class="one_cat_wrapp sub">
                <div class="one_cat col-xs-12">
                    {{$category->name}}
                    <span class="tours_count">({{count($category->tour)}})</span>
                    <div class="categories_controls">
                        <a href="/admin/tours/category/{{$category->id}}/edit" class="edit">
                            <span class="text">Edit</span>
                            <i class="fas fa-edit"></i>
                        </a>
                        <a href="/admin/tours/category/{{$category->id}}/delete" class="delete">
                            <span class="text">Delete</span>
                            <i class="fas fa-trash-alt"></i>
                        </a>
                    </div>
                </div>

                @if ( count($category->childs()->get()) > 0 )
                    @include('admin.templates.tours.categories.one-cat')
                @endif

            </div>

        @endforeach
    @endif

