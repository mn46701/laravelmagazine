<div class="tit">City {{$event}}</div>
<select name="city_{{$event}}" id="city_{{$event}}">
    <option value="0">Select city</option>
    @foreach($cities as $city)
        <option value="{{$city->city_id}}">{{$city->name}}</option>
    @endforeach
</select>