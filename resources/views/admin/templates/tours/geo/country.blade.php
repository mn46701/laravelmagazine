<div class="tit">Country {{$event}}</div>
<select name="country_{{$event}}" id="country_{{$event}}">
    <option value="0">Select country</option>
    @foreach($countriesAll as $country)
        <option value="{{$country->country_id}}">{{$country->name}}</option>
    @endforeach
</select>