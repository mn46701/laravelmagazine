<div class="col-xs-12 position_relative">
    <div class="tour_filtration">
        {{ Form::open( ['route' => 'tours-filtration', 'method' => 'GET'] ) }}

        <select name="category" id="">
            @if(isset($_GET['category']))
                @if($_GET['category'] == 0)
                    <option value="0" selected>Select category</option>
                @else
                    <option value="0">Select category</option>
                @endif
                @foreach($allCategories as $category)
                    @if($category->id == $_GET['category'])
                        <option value="{{$category->id}}" selected>{{$category->name}}</option>
                    @else
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endif
                @endforeach
            @else
                <option value="0">Select category</option>
                @foreach($allCategories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            @endif
        </select>
        @php
            if(isset($_GET['price_from']))
            {
                $price_from = $_GET['price_from'];
            } else {
                $price_from = App\Tour::min('price') - 1;
            }
            if(isset($_GET['price_to']))
            {
                $price_to = $_GET['price_to'];
            } else {
                $price_to = App\Tour::max('price') + 1;
            }
            if(isset($_GET['search']))
            {
                $search = $_GET['search'];
            } else {
                $search = '';
            }
        @endphp

        <label for="price_from">Min price</label>
        <input type="number" name="price_from" id="price_from" placeholder="Min price" min="0"
               value="{{$price_from}}">
        <label for="price_to">Max price</label>
        <input type="number" name="price_to" id="price_to" placeholder="Max price" min="100"
               value="{{$price_to}}">


        <input type="submit" value="Filter" class="btn btn-default">

        <a href="{{route('admintours')}}" class="btn btn-danger">Reset</a>

        {{Form::close()}}
    </div>

    <div class="tour_search">
        {{ Form::open( ['route' => 'tours-search', 'method' => 'GET'] ) }}
        <input type="text" name="search" placeholder="Search" value="{{$search}}" class="search_input">
        <input type="submit" value="Search" class="btn btn-default">
        {{Form::close()}}
    </div>

</div>