@extends('admin.layouts.admin')
@section('content')

    <div class="all_tours_wrapper">
        <h3>{{$title}}</h3>

        @include('admin.templates.tours.filtration-block')

        @if(count($tours) == 0)
            <div class="alert alert-danger no_tours_mes">
                <div>There are not anough tours. . .</div>
                <div>
                    <a href="{{route('create-tour')}}" class="btn btn-default">Create Tour</a>
                </div>
            </div>
        @else
            @foreach($tours as $tour)
                <div class="col-md-3 one_tour_wrapper">
                    @include('admin.templates.tours.one-tour')
                </div>
            @endforeach
        @endif
    </div>

@endsection