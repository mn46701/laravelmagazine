@extends('admin.layouts.admin')

@section('content')

    @if (count($errors) > 0)
        <ul class="errors_ul">
            @foreach ($errors->all() as $error)
                <li class="alert alert-danger">{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <div class="col-xs-12">
        <h1 class="margin_bottom">Editing user</h1>
    </div>

    <div class="col-xs-12">
        {{Form::open(['route' => ['userStore', 'id' => $user->id], 'method' => 'POST', 'id' => 'user_edit_form'])}}

        <label for="">Name</label>
        <input type="text" name="name" value="{{$user->name}}">
        <label for="">Lastname</label>
        <input type="text" name="lastname" value="{{$user->lastname}}">
        <label for="">Phone</label>
        <input type="number" name="phone" value="{{$user->phone}}">
        <label for="">Country</label>
        <input type="text" name="country" value="{{$user->country}}">
        <label for="">City</label>
        <input type="text" name="city" value="{{$user->city}}">
        <label for="">Email</label>
        <input type="email" name="email" value="{{$user->email}}">

        <select name="user_role_id" id="">
            @if($user->user_role_id == 1)
                <option value="{{$user->user_role_id}}" selected>Administrator</option>
                <option value="2">User</option>
            @else
                <option value="{{$user->user_role_id}}" selected>User</option>
                <option value="1">Administrator</option>
            @endif
        </select>

        <input type="submit" class="btn btn-default" value="Save">

        {{Form::close()}}
    </div>







@endsection