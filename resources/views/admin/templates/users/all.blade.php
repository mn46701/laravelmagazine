@extends('admin.layouts.admin')

@section('content')

    @if (count($errors) > 0)
        <ul class="errors_ul">
            @foreach ($errors->all() as $error)
                <li class="alert alert-danger">{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <div class="col-xs-12">
        <h1 class="margin_bottom">Users</h1>
    </div>

    @foreach($users as $user)

        <div class="col-xs-12">
            <div class="one_user">
                <div class="user_name">
                    {{$user->name}}
                </div>

                <div class="user_controls">

                    <a href="/admin/user/{{$user->id}}/edit" class="edit">
                        <span class="text">Edit</span>
                        <i class="fas fa-edit"></i>
                    </a>
                    <a href="/admin/user/{{$user->id}}/delete" class="delete">
                        <span class="text">Delete</span>
                        <i class="fas fa-trash-alt"></i>
                    </a>

                </div>

            </div>
        </div>

    @endforeach






@endsection